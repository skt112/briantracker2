package com.chivsp.briantracker;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.util.Log;

public class CustomOpenHelper extends SQLiteOpenHelper {


    // データベースのバージョン(2,3と挙げていくとonUpgradeメソッドが実行される)
    static final private int VERSION = 1;

    // コンストラクタ　以下のように呼ぶこと
    public CustomOpenHelper(Context context){
        super(context, "Briantracker_DB", null, VERSION);
    }

    // データベースが作成された時に実行される処理
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*
          テーブルを作成する
          execSQLメソッドにCREATET TABLE命令を文字列として渡すことで実行される
          引数で指定されているものの意味は以下の通り
          引数1 ・・・ id：列名 , INTEGER：数値型 , PRIMARY KEY：テーブル内の行で重複無し , AUTOINCREMENT：1から順番に振っていく
          引数2 ・・・ name：列名 , TEXT：文字列型
          引数3 ・・・ price：列名 , INTEGER：数値型
         */
        db.execSQL("CREATE TABLE PLAYER_INFO ( " +
                "PLAYER_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "PLAYER_NAME TEXT , " +
                "HAND_NUM INTERGER, " +
                "ACTION_NUM INTERGER, " +
                "RAISE_NUM INTERGER, " +
                "THREE_BET_NUM INTERGER, " +
                "FOUR_BET_NUM INTERGER, " +
                "CB_NUM INTERGER)");
        Log.d("debug", "onCreate(SQLiteDatabase db)");
    }

    // データベースをバージョンアップした時に実行される処理
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // アップデートの判別、古いバージョンは削除して新規作成
        db.execSQL(
                "DROP TABLE IF EXISTS PLAYER_INFO"
        );
        onCreate(db);
    }

    public static  void saveData(SQLiteDatabase db, String PLAYER_NAME){
        ContentValues values = new ContentValues();
        values.put("PLAYER_NAME", PLAYER_NAME);
        values.put("HAND_NUM", 8);
        values.put("ACTION_NUM", 100);
        values.put("RAISE_NUM ", 2);
        values.put("THREE_BET_NUM", 3);
        values.put("FOUR_BET_NUM", 1);
        values.put("CB_NUM", 0);


        db.insert("PLAYER_INFO", null, values);
    }

    public void onDowngrade(SQLiteDatabase db,
                            int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    // データベースが開かれた時に実行される処理
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}