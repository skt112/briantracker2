package com.chivsp.briantracker;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

public class MainActivity extends AppCompatActivity {

    private BottomSheetDialog dialog;
    private CustomOpenHelper helper;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText nameText = findViewById(R.id.editName);

        Button entryButton =  findViewById(R.id.Next);
        entryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(helper == null){
                    helper = new CustomOpenHelper(getApplicationContext());
                }

                if(db == null){
                    db = helper.getWritableDatabase();
                }
                String name = nameText.getText().toString();
                CustomOpenHelper.saveData(db , name);

            }
        });

        Button btnPlayer1 = findViewById(R.id.PlayerButton1);
        btnPlayer1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer2 = findViewById(R.id.PlayerButton2);
        btnPlayer2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player2が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer3 = findViewById(R.id.PlayerButton3);
        btnPlayer3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer4 = findViewById(R.id.PlayerButton4);
        btnPlayer4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer5 = findViewById(R.id.PlayerButton5);
        btnPlayer5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer6 = findViewById(R.id.PlayerButton6);
        btnPlayer6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer7 = findViewById(R.id.PlayerButton7);
        btnPlayer7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer8 = findViewById(R.id.PlayerButton8);
        btnPlayer8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer9 = findViewById(R.id.PlayerButton9);
        btnPlayer9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

        Button btnPlayer10 = findViewById(R.id.PlayerButton10);
        btnPlayer10.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                openDialog((Button)v);
            }
        });

    }
    // オプションメニューを作成する
    public boolean onCreateOptionsMenu(Menu menu){
        // menuにcustom_menuレイアウトを適用
        getMenuInflater().inflate(R.menu.custom_menu_main, menu);
        // オプションメニュー表示する場合はtrue
        return true;
    }
    // メニュー選択時の処理
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Intent intent;

        // 押されたメニューのIDで処理を振り分ける
        if (menuItem.getItemId() == R.id.action_disp_player) {// インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(MainActivity.this, SubActivity.class);
            // Activity起動
            startActivity(intent);
        }else {
            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(MainActivity.this, MainActivity.class);
            // Activity起動
            startActivity(intent);
        }

        return true;
    }

    private void openDialog(final Button btn) {
        View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet, null);
        dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);

        ImageButton btnFold = (ImageButton) view.findViewById(R.id.btnFold);
        btnFold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Foldが押されました",Toast.LENGTH_SHORT).show();
                btn.setText("F");
                dialog.dismiss();
            }
        });

        ImageButton btnCall = (ImageButton) view.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Callが押されました",Toast.LENGTH_SHORT).show();
                btn.setText("C");
                dialog.dismiss();
            }
        });

        ImageButton btnRaise = (ImageButton) view.findViewById(R.id.btnRaise);
        btnRaise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Raiseが押されました",Toast.LENGTH_SHORT).show();
                btn.setText("R");
                dialog.dismiss();
            }
        });

        ImageButton btnThree = (ImageButton) view.findViewById(R.id.btnThree);
        btnThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Threeが押されました",Toast.LENGTH_SHORT).show();
                btn.setText("3");
                dialog.dismiss();
            }
        });

        ImageButton btnFour = (ImageButton) view.findViewById(R.id.btnFour);
        btnFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Fourが押されました",Toast.LENGTH_SHORT).show();
                btn.setText("4");
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
