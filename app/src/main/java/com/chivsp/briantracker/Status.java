package com.chivsp.briantracker;

public enum Status {

    Fold("F"),
    Call("C"),
    Rais("R"),
    Three("3"),
    For("4");
    private String value;

    Status(String v){
        value = v;
    }

    public String value() {
        return value;
    }
}
