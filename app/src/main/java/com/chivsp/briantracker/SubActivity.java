package com.chivsp.briantracker;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.TextView;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SubActivity extends AppCompatActivity {

    private TextView nameView;
    private TextView HANDView;
    private TextView VPIPView;
    private TextView PFRView;
    private TextView threeBetView;
    private TextView fourBetView;
    private TextView CB_View;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);


        nameView = findViewById(R.id.name_view);
        HANDView = findViewById(R.id.HAND_view);
        VPIPView = findViewById(R.id.VPIP_view);
        PFRView = findViewById(R.id.PFR_view);
        threeBetView = findViewById(R.id.threeBet_view);
        fourBetView = findViewById(R.id.fourBet_view);
        CB_View = findViewById(R.id.CB_view);

        CustomOpenHelper helper = new CustomOpenHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();

        // queryメソッドの実行例
        Cursor c = db.query("PLAYER_INFO", new String[] {"PLAYER_ID","PLAYER_NAME","HAND_NUM","ACTION_NUM","RAISE_NUM","THREE_BET_NUM","FOUR_BET_NUM","CB_NUM"}, null,
                null, null, null, null);

        c.moveToFirst();

        String disp_playerName = "プレイヤー名";
        String disp_handNum = "HAND数";
        String disp_vrip = "VPIP";
        String disp_pfr = "PFR";
        String disp_three = "3Bet率";
        String disp_four = "4Bet+率";
        String disp_cb = "CB率";

        for (int i = 0; i < c.getCount(); i++) {
            String playerName = c.getString(1);//PLAYER_NAME
            int handNum = c.getInt(2);//HAND_NUM
            int actionNum = c.getInt(3);//ACTION_NUM
            int raiseNum = c.getInt(4);//RAISE_NUM
            int threeBetNum = c.getInt(5);//THREE_BET_NUM
            int fourBetNum = c.getInt(6);//FOUR_BET_NUM
            int cbNum = c.getInt(7);//CB_NUM

            disp_playerName = disp_playerName + "\n" + playerName;
            disp_handNum = disp_handNum + "\n" + handNum;
            disp_vrip = disp_vrip + "\n" + keisan.getVpip(handNum, actionNum);
            disp_pfr = disp_pfr+ "\n" + keisan.getPreRaise(handNum, raiseNum) ;
            disp_three = disp_three + "\n" + keisan.getThreeBet(handNum, threeBetNum);
            disp_four = disp_four + "\n" + keisan.getForBetPlus(handNum, fourBetNum);
            disp_cb = disp_cb + "\n" + keisan.getCBet(raiseNum, cbNum);

            c.moveToNext();
        }

        c.close();
        db.close();
        nameView.setText(disp_playerName);//プレイヤー名
        HANDView.setText(disp_handNum);//HAND数
        VPIPView.setText(disp_vrip);//VPIP
        PFRView.setText(disp_pfr );//PFR
        threeBetView.setText(disp_three);//3Bet率
        fourBetView.setText(disp_four);//4Bet+率
        CB_View.setText(disp_cb);//CB率
    }

    // オプションメニューを作成する
    public boolean onCreateOptionsMenu(Menu menu){
        // menuにcustom_menuレイアウトを適用
        getMenuInflater().inflate(R.menu.custom_menu_second, menu);
        // オプションメニュー表示する場合はtrue
        return true;
    }
    // メニュー選択時の処理
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Intent intent;

        // 押されたメニューのIDで処理を振り分ける
        //アクション登録画面
        if (menuItem.getItemId() == R.id.action_register_action) {
            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(SubActivity.this, MainActivity.class);
            // Activity起動
            startActivity(intent);
        //新規登録
        }else {
            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(SubActivity.this, MainActivity.class);
            // Activity起動 新規登録
            startActivity(intent);
        }
        return true;
    }
}
