package com.chivsp.briantracker;

public class Player {

    /** プレイヤー名 */
    private String playerName ="";
    /** ハンド数 */
    private int handSu = 0;
    /** 参加数 */
    private int sankaSu = 0;
    /** レイズ数 */
    private int raiseSu = 0;
    /** ３bet数 */
    private int threeBetSu = 0;
    /** ４bet+数 */
    private int forBetPlusSu = 0;
    /** シートNo. */
    private int seatNum = 0;
    /** コンテニューションbet数 */
    private int cBetSu = 0;

    /**
     * コンストラクタ
     * プレイヤー名がある場合のコンストラクタ
     *
     * @param seatNum
     * @param playerName
     */
    public Player(int seatNum,String playerName){
        this.seatNum = seatNum;

        //プレイヤー名が無い場合はUnknownで作成
        if(playerName == null || "".equals(playerName)){
            this.playerName = "Unknown" + seatNum;
        }else{
            this.playerName = playerName;
        }

        //playerNameから該当するPlayerの検索して値をセット（未実装）

    }

    /**
     * CNextの場合の各数値の計算を行い、DB登録を行う。
     *
     * @param Status プレイヤーのアクション
     */
    public void cNext(Status status){
        count(status);

        if(status.equals(Status.Fold))return;
        if(status.equals(Status.Call))return;
        cBetSu++;
        //計算結果をDB登録する。（未実装）
    }

    /**
     * nextの場合の各数値の計算を行い、DB登録を行う。
     *
     * @param Status プレイヤーのアクション
     */
    public void next(Status status){
        count(status);
        //計算結果をDB登録する。（未実装）
    }

    //各数値の計算共通処理
    private void count(Status status){
        handSu++;

        if(status.equals(Status.Fold))return;

        sankaSu++;
        if(status.equals(Status.Call))return;

        raiseSu++;
        if(status.equals(Status.Rais))return;

        threeBetSu++;
        if(status.equals(Status.Three))return;

        forBetPlusSu++;

    }


    /**
     * プレイヤーの表示用の統計情報を取得
     *
     */
    public String getStatistics(){

        String disp=this.playerName + "\r\n";
        disp = disp + "H:" + handSu + "/";
        disp = disp + "V:" + getVpip() + "/";
        disp = disp + "P:" + getPreRaise() + "\r\n";
        disp = disp + "3:" + getThreeBet() + "/";
        disp = disp + "4:" + getForBetPlus() + "/";
        disp = disp + "C:" + getCBet();

        return disp;
    }

    // VPIPの値を取得
    private int getVpip(){
        if(this.handSu == 0)return 0;
        double calc = (double)this.sankaSu /this.handSu * 100;
        return (int)calc;
    }

    // プリフロップレイズ率の値を取得
    private int getPreRaise(){
        if(this.handSu == 0)return 0;
        double calc = (double)this.raiseSu / this.handSu * 100;
        return (int)calc;
    }

    // 3bet率の値を取得
    private int getThreeBet(){
        if(this.handSu == 0)return 0;
        double calc = (double)this.threeBetSu / this.handSu * 100;
        return (int)calc;
    }

    // 4bet+率の値を取得
    private int getForBetPlus(){
        if(this.handSu == 0)return 0;
        double calc = (double)this.forBetPlusSu / this.handSu * 100;
        return (int)calc;
    }

    // Cbet+率の値を取得
    private int getCBet(){
        if(this.raiseSu == 0)return 0;
        double calc = (double)this.cBetSu / this.raiseSu * 100;
        return (int)calc;
    }

    public int getHandSu() {
        return handSu;
    }

    public int getSankaSu() {
        return sankaSu;
    }

    public int getRaisSu() {
        return raiseSu;
    }

    public int getThreeBetSu() {
        return threeBetSu;
    }

    public int getForBetPlusSu() {
        return forBetPlusSu;
    }

    public int getSeatNum() {
        return seatNum;
    }

    public int getcBetSu() {
        return cBetSu;
    }

    public String getPlayerName(){
        return playerName;
    }

}
